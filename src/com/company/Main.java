package com.company;

import java.sql.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws SQLException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id of adress:");
        String scanId = scanner.next();
        System.out.println("Enter number of column:");
        String scanColumn = scanner.next();
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3308/levelup", "root", "123456");
        PreparedStatement preparedStatement = connection.prepareStatement("select * from adress where id = ?");
        preparedStatement.setInt(1, Integer.parseInt(scanId));
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()){
            System.out.println(resultSet.getString(Integer.parseInt(scanColumn)));
        }
    }
}
